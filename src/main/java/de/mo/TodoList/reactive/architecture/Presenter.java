package de.mo.TodoList.reactive.architecture;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by buzzer on 11.02.16.
 */
public abstract class Presenter<T> {

    public PublishSubject<ActionDTO> bus;

    protected Observable<T> state;

    public Presenter(T initalValue) {
		bus = PublishSubject.create();
    	if(initalValue != null) {
    		this.state = this.bus.scan(initalValue, this::update);
    	}
    }

    public abstract T update(T model, ActionDTO action);

    public void doAction(ActionDTO action) {
        this.bus.onNext(action);
    }
    
    public Observable<T> getState() {
		return state;
	}

}