package de.mo.TodoList.model;

import java.io.Serializable;

public class Todo implements Serializable {

	private static final long serialVersionUID = 7550988664687618116L;

	public static int count = 0;
	
	private String todoText;
	
	private boolean completed;
	
	private int id;
	
	public Todo() {
		this.id = count++;
	}

	public Todo(String todoText) {
		this();
		this.todoText = todoText;
	}

	public String getTodo() {
		return todoText;
	}

	public void setTodo(String todo) {
		this.todoText = todo;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void toggleCompleted() {
		this.completed = !this.completed;
	}
	
}
