package de.mo.TodoList.reactive;

import java.util.function.Consumer;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Property.ValueChangeNotifier;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
import rx.functions.Func1;
import rx.subscriptions.Subscriptions;

public class VaadinObservable {
	
	public static Observable<ValueChangeEvent> fromValueChangeEvent(ValueChangeNotifier changeNotifier) {
		return Observable.<ValueChangeEvent>create(subscriber -> {
			ValueChangeListener listener =  event -> subscriber.onNext(event);
			
			changeNotifier.addValueChangeListener(listener);
			subscriber.add(Subscriptions.create(new Action0() {

				@Override
				public void call() {
					changeNotifier.removeValueChangeListener(listener);
				}
				
			}));
			
		});
	}
	
	public static Observable<ClickEvent> fromClickEvent(Button submitButton) {
		return Observable.<ClickEvent>create(subscriber -> {
			@SuppressWarnings("serial")
			ClickListener listener = new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					subscriber.onNext(event);
				}
			};
			
			submitButton.addClickListener(listener);
			subscriber.add(Subscriptions.create(new Action0() {

				@Override
				public void call() {
					submitButton.removeClickListener(listener);
				}
				
			}));
			
		});
	}

	public static Observable<ItemClickEvent> fromItemClickEvent(Grid grid) {
		return Observable.<ItemClickEvent>create(subscriber -> {
			@SuppressWarnings("serial")
			ItemClickListener listener = new ItemClickListener() {
				
				@Override
				public void itemClick(ItemClickEvent event) {
					subscriber.onNext(event);
				}

			};
			
			grid.addItemClickListener(listener);
			subscriber.add(Subscriptions.create(new Action0() {

				@Override
				public void call() {
					grid.removeItemClickListener(listener);
				}
				
			}));
			
		});
	}

	public static <T> Observable<T> fromConsumer(Func1<Consumer<T>, Void> addListener) {
		return Observable.<T>create(subscriber -> {
			Consumer<T> consumer = (T t) -> subscriber.onNext(t);
			addListener.call(consumer);
		});		
	}
	

}
