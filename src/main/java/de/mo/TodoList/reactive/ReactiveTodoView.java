package de.mo.TodoList.reactive;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.renderers.ButtonRenderer;

import de.mo.TodoList.MyUI;
import de.mo.TodoList.model.Todo;
import de.mo.TodoList.model.TodoModel;
import rx.Observable;

public class ReactiveTodoView extends HorizontalLayout {
	
	private static final long serialVersionUID = 8341409499458846292L;
	
	private Button submitButton = new Button("Submit");
	
	private TextField inputField = new TextField();

	private Grid grid;

	private ObservableContainer messageContainer; 
	
	private List<Consumer<Todo>> deleteListener = new ArrayList<Consumer<Todo>>();
	
    public ReactiveTodoView(MyUI ui) {
    	setStyleName("main-screen");

        CssLayout viewContainer = new CssLayout();
        viewContainer.setSizeFull();
        addComponent(viewContainer);
        setExpandRatio(viewContainer, 1);
        
        HorizontalLayout topBar = new HorizontalLayout();
        topBar.addComponent(inputField);
        topBar.addComponent(submitButton);
        topBar.setWidth("100%");
        viewContainer.addComponent(topBar);

        this.grid = buildChatGrid();
        viewContainer.addComponent(this.grid);
        setSizeFull();
    }
    
    private Grid buildChatGrid() {
    	Grid grid = new Grid();
    	grid.setColumns("todo", "completed");
    	grid.addSelectionListener(listener -> grid.deselect(grid.getSelectedRow()));
    	this.messageContainer = new ObservableContainer<>(Todo.class);
    	// add delete row
    	GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(messageContainer);
    	gpc.addGeneratedProperty("delete", new PropertyValueGenerator<String>() {

			@Override
			public String getValue(Item item, Object itemId, Object propertyId) {
				return "delete";
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
    		
    	});
    	grid.setContainerDataSource(gpc);
    	grid.setColumns("todo", "completed", "delete");
    	grid.getColumn("delete")
			.setRenderer(new ButtonRenderer(click -> fireDeleteEvent((Todo) click.getItemId()), "delete"));
    	
    	return grid;
    }
    
    public void setTodoStream(Observable<TodoModel> todoObservable) {
    	messageContainer.setObservable(todoObservable.map(model -> model.todos));
    }
    
    public Observable<ClickEvent> getSubmitObervable() {
    	return VaadinObservable.fromClickEvent(this.submitButton);
    }
    
    public Observable<ValueChangeEvent> getMessageObservable() {
    	return VaadinObservable.fromValueChangeEvent(this.inputField);
    }
    
    public Observable<Todo> getToggledTodo() {
    	return VaadinObservable.fromItemClickEvent(this.grid)
    			.map(event -> (Todo) event.getItemId());
    }
    
    public Observable<Todo> getDeleteTodo() {
    	return Observable.<Todo>create(subscriber -> {
			Consumer<Todo> consumer = todo -> subscriber.onNext(todo);
			this.deleteListener.add(consumer);
		});		
    }
    
    private void fireDeleteEvent(Todo todo) {
		for(Consumer<Todo> listener : this.deleteListener) {
			listener.accept(todo);
		}
	}
    
}
