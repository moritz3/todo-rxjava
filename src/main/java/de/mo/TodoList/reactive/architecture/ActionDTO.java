package de.mo.TodoList.reactive.architecture;

/**
 * Created by buzzer on 11.02.16.
 */
public class ActionDTO<T> {
    public Action action;
    public T payload;
    public Class<T> payloadType;

    public ActionDTO(Action action, T payload, Class<T> payloadType) {
        this.action = action;
        this.payload = payload;
        this.payloadType = payloadType;
    }

    public ActionDTO(Action action) {
        this.action = action;
    }

}