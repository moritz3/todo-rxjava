package de.mo.TodoList.reactive;

import java.util.Collection;

import com.vaadin.data.util.BeanItemContainer;

import rx.Observable;

@SuppressWarnings("serial")
public class ObservableContainer<T> extends BeanItemContainer<T>{

	public ObservableContainer(Class<? super T> type) throws IllegalArgumentException {
		super(type);
	}
	
	public ObservableContainer(Class<? super T> type, Observable<Collection<? extends T>> observable) throws IllegalArgumentException {
		super(type);
		this.setObservable(observable);
	}
	
	public void setObservable(Observable<Collection<? extends T>> observable) {
		observable.subscribe(items -> {
				this.removeAllItems();
				this.addAll(items);
		});
	}
	
}
