package de.mo.TodoList.classic;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.renderers.ButtonRenderer;

import de.mo.TodoList.MyUI;
import de.mo.TodoList.model.Todo;

public class TodoView extends HorizontalLayout {
	
	private static final long serialVersionUID = 8341409499458846292L;
	
	private Button submitButton = new Button("Submit");
	
	private TextField inputField = new TextField();
	
	private List<Consumer<Todo>> deleteListener = new ArrayList<Consumer<Todo>>();

	private Grid grid; 
	
    public TodoView(MyUI ui) {
    	setStyleName("main-screen");

        CssLayout viewContainer = new CssLayout();
        viewContainer.setSizeFull();
        viewContainer.setWidth(500, Unit.PIXELS);
        addComponent(viewContainer);
        setExpandRatio(viewContainer, 1);
        
        HorizontalLayout topBar = new HorizontalLayout();
        inputField.setWidth(300, Unit.PIXELS);
        topBar.addComponent(inputField);
        topBar.addComponent(submitButton);
        viewContainer.addComponent(topBar);

        this.grid = buildTodoGrid();
        viewContainer.addComponent(this.grid);
        setSizeFull();
    }
    
    private Grid buildTodoGrid() {
    	Grid grid = new Grid();
    	grid.addSelectionListener(listener -> grid.deselect(grid.getSelectedRow()));

    	// create container
    	BeanItemContainer<Todo> container = new BeanItemContainer<>(Todo.class);
    	
    	// add delete row
    	GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(container);
    	gpc.addGeneratedProperty("delete", new PropertyValueGenerator<String>() {

			@Override
			public String getValue(Item item, Object itemId, Object propertyId) {
				return "delete";
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
    		
    	});
    	grid.setContainerDataSource(gpc);
    	grid.setColumns("todo", "completed", "delete");
    	grid.getColumn("delete")
    		.setRenderer(new ButtonRenderer(click -> fireDeleteEvent((Todo) click.getItemId()), "delete"));
    	return grid;
    }
    
    public void addSelectionListener(SelectionListener listener) {
    	this.grid.addSelectionListener(listener);
    }
    
    public void addSumbitListener(ClickListener listener) {
    	this.submitButton.addClickListener(listener);
    }
    
    public void addInputChangeListener(ValueChangeListener listener) {
    	this.inputField.addValueChangeListener(listener);
    }
    
    public void addTodo(Todo todo) {
    	this.grid.getContainerDataSource().addItem(todo);
    }
    
    public void removeTodo(Todo todo) {
    	this.grid.getContainerDataSource().removeItem(todo);
    }

	public void addDeleteListener(Consumer<Todo> deleteListener) {
		this.deleteListener.add(deleteListener);
	}
	
	private void fireDeleteEvent(Todo todo) {
		for(Consumer<Todo> listener : this.deleteListener) {
			listener.accept(todo);
		}
	}
    
}
